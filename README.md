Source the included `splunk` file to enable command completions on the Splunk CLI; automate via a `.bash_profile` or by placing in your system's `bash_completion.d`.
